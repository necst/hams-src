/*
 * Copyright 2016 Chiara Gatti, Guido Lanfranchi, Gabriele Pallotta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/



function [c, s] = oneJAC (A, i, j)

alpha = 2*sum(A(:,i).*A(:,j));
beta = sum(A(:,j).^2);
gamma = sqrt(alpha^2+beta^2);

if beta>0
    c = sqrt((beta+gamma)/(2*gamma));
    s = alpha/(2*gamma*c);
else if beta<0
        s = sqrt((gamma-beta)/(2*gamma));
        c = alpha/(2*gamma*s);
        disp('WARNING');
    end
end

return


