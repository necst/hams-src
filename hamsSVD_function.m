/*
 * Copyright 2016 Chiara Gatti, Guido Lanfranchi, Gabriele Pallotta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/



%%Input matrice A lineare da invertire e coppie da prendere
%%Output matrici U1 V1 linearizzate

function [U1,V1]=hamsSVD_function(A, pairs_1,pairs_2)

U1=A;
N=length(A);
n=sqrt(N);
V1_nonlin=eye(n);
V1 = zeros(1,N);

for i=1:n
    for j=1:n
        V1((i-1)*n+j)=V1_nonlin(i,j);
    end
end

nsweeps=8;
ct1=0;
st1=0;

for m=1:nsweeps
    
    for i=1:n-1
        for j=1:n/2
        
            % alpha = 2*sum(U1(pairs_1((i-1)*n/2+j)).*U1(pairs_2((i-1)*n/2+j)));
            % beta = sum(U1(pairs_2((i-1)*n/2+j)).^2);
            alpha = 0;
            beta = 0;
            for k=1:n % n columns for matrix U
                alpha = alpha + 2 * U1((k-1)*n+pairs_1((i-1)*n/2+j)) * U1((k-1)*n+pairs_2((i-1)*n/2+j));
                beta = beta + U1((k-1)*n+pairs_2((i-1)*n/2+j)).^2;
            end
            
            gamma = sqrt(alpha^2+beta^2);
           
          
            ct1 = sqrt((beta+gamma)/(2*gamma));
            st1 = alpha/(2*gamma*ct1);
            
            
            for k=1:n % n columns for matrix U
                t=U1((k-1)*n+pairs_1((i-1)*n/2+j));
                U1((k-1)*n+pairs_1((i-1)*n/2+j))=ct1*t-st1*U1((k-1)*n+pairs_2((i-1)*n/2+j));
                U1((k-1)*n+pairs_2((i-1)*n/2+j))=st1*t+ct1*U1((k-1)*n+pairs_2((i-1)*n/2+j));

                t=V1((k-1)*n+pairs_1((i-1)*n/2+j));
                V1((k-1)*n+pairs_1((i-1)*n/2+j))=ct1*t-st1*V1((k-1)*n+pairs_2((i-1)*n/2+j));
                V1((k-1)*n+pairs_2((i-1)*n/2+j))=st1*t+ct1*V1((k-1)*n+pairs_2((i-1)*n/2+j));
            end
        
        end 
    end
end

end
