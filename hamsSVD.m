/*
 * Copyright 2016 Chiara Gatti, Guido Lanfranchi, Gabriele Pallotta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/




A = magic(4);

U1 = A;
V1 = eye(4);

nsweeps = 8;

for m = 1:nsweeps
    
  %% (1,2) e (3,4)
    
    % ct1 e st1 per (1,2)
    alpha = 2*sum(U1(:,1).*U1(:,2));
    beta = sum(U1(:,2).^2);
    gamma = sqrt(alpha^2+beta^2);
    if beta>0
        ct1 = sqrt((beta+gamma)/(2*gamma));
        st1 = alpha/(2*gamma*ct1);
    else if beta<0
        st1 = sqrt((gamma-beta)/(2*gamma));
        ct1 = alpha/(2*gamma*st1);
        disp('fanculo');
        end
    end

    % ct2 e st2 per (3,4)
    alpha = 2*sum(U1(:,3).*U1(:,4));
    beta = sum(U1(:,4).^2);
    gamma = sqrt(alpha^2+beta^2);
    if beta>0
        ct2 = sqrt((beta+gamma)/(2*gamma));
        st2 = alpha/(2*gamma*ct2);
    else if beta<0
        st2 = sqrt((gamma-beta)/(2*gamma));
        ct2 = alpha/(2*gamma*st2);
        disp('WARNING');
        end
    end

    % aggiornamento
    t=U1(:,1);
    U1(:,1)=ct1*t-st1*U1(:,2);
    U1(:,2)=st1*t+ct1*U1(:,2);

    t=V1(:,1);
    V1(:,1)=ct1*t-st1*V1(:,2);
    V1(:,2)=st1*t+ct1*V1(:,2);

    t=U1(:,3);
    U1(:,3)=ct2*t-st2*U1(:,4);
    U1(:,4)=st2*t+ct2*U1(:,4);

    t=V1(:,3);
    V1(:,3)=ct2*t-st2*V1(:,4);
    V1(:,4)=st2*t+ct2*V1(:,4);

  %% (1,3) e (2,4)
    
    % ct1 e st1 per (1,3)
    alpha = 2*sum(U1(:,1).*U1(:,3));
    beta = sum(U1(:,3).^2);
    gamma = sqrt(alpha^2+beta^2);
    if beta>0
        ct1 = sqrt((beta+gamma)/(2*gamma));
        st1 = alpha/(2*gamma*ct1);
    else if beta<0
        st1 = sqrt((gamma-beta)/(2*gamma));
        ct1 = alpha/(2*gamma*st1);
        disp('WARNING');
        end
    end

    % ct2 e st2 per (2,4)
    alpha = 2*sum(U1(:,2).*U1(:,4));
    beta = sum(U1(:,4).^2);
    gamma = sqrt(alpha^2+beta^2);
    if beta>0
        ct2 = sqrt((beta+gamma)/(2*gamma));
        st2 = alpha/(2*gamma*ct2);
    else if beta<0
        st2 = sqrt((gamma-beta)/(2*gamma));
        ct2 = alpha/(2*gamma*st2);
        disp('WARNING');
        end
    end

    % aggiornamento
    t=U1(:,1);
    U1(:,1)=ct1*t-st1*U1(:,3);
    U1(:,3)=st1*t+ct1*U1(:,3); 

    t=V1(:,1);
    V1(:,1)=ct1*t-st1*V1(:,3);
    V1(:,3)=st1*t+ct1*V1(:,3);

    t=U1(:,2);
    U1(:,2)=ct2*t-st2*U1(:,4);
    U1(:,4)=st2*t+ct2*U1(:,4);

    t=V1(:,2);
    V1(:,2)=ct2*t-st2*V1(:,4);
    V1(:,4)=st2*t+ct2*V1(:,4);

  %% (1,4) e (2,3)
  
    % ct1 e st1 per (1,4)
    alpha = 2*sum(U1(:,1).*U1(:,4));
    beta = sum(U1(:,4).^2);
    gamma = sqrt(alpha^2+beta^2);
    if beta>0
        ct1 = sqrt((beta+gamma)/(2*gamma));
        st1 = alpha/(2*gamma*ct1);
    else if beta<0
        st1 = sqrt((gamma-beta)/(2*gamma));
        ct1 = alpha/(2*gamma*st1);
        disp('WARNING');
        end
    end

    % ct2 e st2 per (2,3)
    alpha = 2*sum(U1(:,2).*U1(:,3));
    beta = sum(U1(:,3).^2);
    gamma = sqrt(alpha^2+beta^2);
    if beta>0
        ct2 = sqrt((beta+gamma)/(2*gamma));
        st2 = alpha/(2*gamma*ct2);
    else if beta<0
        st2 = sqrt((gamma-beta)/(2*gamma));
        ct2 = alpha/(2*gamma*st2);
        disp('WARNING');
        end
    end

    % aggiornamento
    t=U1(:,1);
    U1(:,1)=ct1*t-st1*U1(:,4);
    U1(:,4)=st1*t+ct1*U1(:,4); 

    t=V1(:,1);
    V1(:,1)=ct1*t-st1*V1(:,4);
    V1(:,4)=st1*t+ct1*V1(:,4);

    t=U1(:,2);
    U1(:,2)=ct2*t-st2*U1(:,3);
    U1(:,3)=st2*t+ct2*U1(:,3);

    t=V1(:,2);
    V1(:,2)=ct2*t-st2*V1(:,3);
    V1(:,3)=st2*t+ct2*V1(:,3);
end

singvals = zeros(1,4);

for j=1:4
  singvals(j)=norm(U1(:,j));
  U1(:,j)=U1(:,j)/singvals(j);
end

D1=diag(singvals);

[U, D, V] = svd(A,'econ');
[U1,D1,V1] = adjustSVD(U1,D1,V1);
