/*
 * Copyright 2016 Chiara Gatti, Guido Lanfranchi, Gabriele Pallotta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/



% function name: FindPairs_bis
% input: dim = size of the matrix that has to be analysed
% output: pairs = a 3D-array with: - 1st level = step (from 1 to dim-1)
%                                  - 2nd level = couple (from 1 to dim/2)
%                                  - 3rd level = element (1 or 2)
%         pad = flag variable (if pad=1 --> zero padding required)
%
% ref. https://pdfs.semanticscholar.org/cf5e/afcd87a9fcf1c77cfb431f0b8a8518f11445.pdf

function [pairs, pad] = FindPairs_bis(dim)
    
    % default: no padding required
    pad = 0;

    if mod(dim,2)~=0
        dim = dim+1;
        pad = 1;
    end
    
    % initialize and elaborate Jacobi Ring
    ring = zeros(2,dim/2);
    ring(1,:) = 2:2:dim;
    ring(2,:) = 1:2:dim-1;
    persistent temp;
    persistent count;
    temp = 0;
    count = 0;
    
    % initialize pairs
    pairs = zeros(dim-1,dim/2,2);
    pos = dim/2;
    
    for i=1:dim-1 % 1st level = steps (Ring Jacobi Ordering has only n-1 steps)
        
        % take first step of couples
        for j=1:dim/2 % 2nd level = couples
            pairs(i,j,1) = ring(1,j);
            pairs(i,j,2) = ring(2,j);
        end
        
        % swap the "dim" element
        if count==0
            % this time "dim" is up --> take it down 
            ring(1,pos) = ring(2,pos);
            ring(2,pos) = dim;
            % then set count different from 0
            count = 1;
        else
            % this time "dim" is down --> take it up 
            ring(2,pos) = ring(1,pos);
            ring(1,pos) = dim;
            % then re-set count to 0 and decrease pos
            count = 0;
            pos = pos-1;
        end
        
        % turn the upper vector
        temp = ring(1,1);
        for k=1:dim/2-1
            ring(1,k) = ring(1,k+1);
        end
        ring(1,dim/2) = temp;
 
    
    end
    
    
end
        
        
                
    