/*
 * Copyright 2016 Chiara Gatti, Guido Lanfranchi, Gabriele Pallotta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/



n=4;
A=magic(n);

[pairs,pad]=FindPairs_bis(n);
pairs_1 = pairs(:,:,1);
pairs_2 = pairs(:,:,2);

A_lin=lin(A);
pairs_1_lin=lin(pairs_1);
pairs_2_lin=lin(pairs_2);

[U1_lin,V1_lin]=hamsSVD_function(A_lin,pairs_1_lin,pairs_2_lin);

U1=anti_lin(U1_lin);
V1=anti_lin(V1_lin);

singvals=zeros(1,n);

for j=1:n
    singvals(j)=norm(U1(:,j));
    U1(:,j)=U1(:,j)/singvals(j);
end

D1=diag(singvals);