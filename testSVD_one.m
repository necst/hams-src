/*
 * Copyright 2016 Chiara Gatti, Guido Lanfranchi, Gabriele Pallotta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/



A = magic(4);

U1 = A;
V1 = eye(4);

nsweeps = 8;

for m = 1:nsweeps
    [U1,V1] = svdONE(U1,V1,1,2,3,4);
    [U1,V1] = svdONE(U1,V1,1,3,2,4);
    [U1,V1] = svdONE(U1,V1,1,4,2,3);
end

singvals = zeros(1,4);

for j=1:4
  singvals(j)=norm(U1(:,j));
  U1(:,j)=U1(:,j)/singvals(j);
end

D1=diag(singvals);

[U, D, V] = svd(A,'econ');
[U1,D1,V1] = adjustSVD(U1,D1,V1);
%[U,D,V] = adjustSVD(U,D,V);
