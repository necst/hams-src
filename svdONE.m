/*
 * Copyright 2016 Chiara Gatti, Guido Lanfranchi, Gabriele Pallotta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/


function [U, V] = svdONE(U,V, r1, c1, r2, c2)

[ct1, st1] = oneJAC(U, r1, c1);
[ct2, st2] = oneJAC(U, r2, c2);

t=U(:,r1);
U(:,r1)=ct1*t-st1*U(:,c1);
U(:,c1)=st1*t+ct1*U(:,c1);

t=V(:,r1);
V(:,r1)=ct1*t-st1*V(:,c1);
V(:,c1)=st1*t+ct1*V(:,c1);

t=U(:,r2);
U(:,r2)=ct2*t-st2*U(:,c2);
U(:,c2)=st2*t+ct2*U(:,c2);

t=V(:,r2);
V(:,r2)=ct2*t-st2*V(:,c2);
V(:,c2)=st2*t+ct2*V(:,c2);

return; % END svdP()