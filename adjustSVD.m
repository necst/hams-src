/*
 * Copyright 2016 Chiara Gatti, Guido Lanfranchi, Gabriele Pallotta
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * 	http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/



function [U, D, V] = adjustSVD(U,D,V)

    TOL = norm(D) * eps;
    
    d = diag(D);    
    d(abs(d) < TOL) = 0;

    dPos = find(d<0);

    if mod(length(dPos),2)==1
        %U(:, dPos) = -U(:, dPos);
        V(:, dPos) = -V(:, dPos);
        %U = -U;
        %V = -V;
    end

    D = abs(D);
    [dSorted, sortedIndex] = sort(diag(D), 'descend');

    Q = zeros(size(D));
    for i=1:length(sortedIndex)
        Q(sortedIndex(i), i) = 1;
    end

    D = Q' * D * Q;
    U = U * Q;
    V = V * Q;

    U(:,rank(D)+1:end) = 0;
    V(:,rank(D)+1:end) = 0;
return; % END svdP()
